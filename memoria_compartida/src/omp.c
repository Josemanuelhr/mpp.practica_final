#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <float.h>
#include <omp.h>

#include "../include/platform.h"
#include "../include/task.h"

void generar(int nivel, int *s, int *done) {
    if (s[nivel] != -1)
        done[s[nivel]] = 0;

    do {
        s[nivel]++;
    } while(done[s[nivel]] == 1);

    done[s[nivel]] = 1;

}

int criterio(int nivel, int *s, int *done, Task *tasks) {
    int task_actual = s[nivel];
    int n_deps = tasks[task_actual].n_deps;

    for (int i = 0; i < n_deps; ++i)
    {
        int dep_actual = tasks[task_actual].dep_tasks[i];
        if (done[dep_actual] == 0)
            return 0;
    }

    return 1;
}

int solucion(int nivel, int n_tasks, int *s, int *done, Task *tasks) {
    return (nivel == n_tasks-1) && criterio(nivel, s, done, tasks);
}

int masHermanos(int nivel, int n_tasks, int *s, int *done) {
    for (int i = s[nivel]+1; i < n_tasks; i++)
        if (done[i] == 0)
            return 1;

    return 0;
}

void retroceder(int nivel, int *s, int *done) {
    done[s[nivel]] = 0;
    s[nivel] = -1;
}

Platform *order_by_eficiencia(Platform *platform) {
    int n_devices = platform->n_devices;

    Platform *res = (Platform *) malloc(sizeof(Platform));
    assert(res);
    res->n_devices = n_devices;
    res->devices = (Device *) malloc(n_devices*sizeof(Device));
    assert(res->devices);

    memcpy(res->devices, platform->devices, sizeof(Device)*n_devices);

    int *inserted = (int *) malloc(n_devices*sizeof(int));
    assert(inserted);
    memset(inserted, 0, sizeof(int)*n_devices);

    int menor_cons;
    double cons_act;
    Device dev;

    for (int i = 0; i < n_devices; ++i)
    {
        menor_cons = i;
        for (int j = i+1; j < n_devices; ++j)
        {
            cons_act = res->devices[j].consumption/res->devices[j].inst_core;

            if (cons_act < res->devices[menor_cons].consumption/res->devices[menor_cons].inst_core) {
                menor_cons = j;
            }
        }

        dev = res->devices[i];
        res->devices[i] = res->devices[menor_cons];
        res->devices[menor_cons] = dev;
    }

    free(inserted);

    return res;
    
}

void siguiente_tarea(int *id_tarea_actual, int *soa, int n_tasks) {
    for (int i = 0; i < n_tasks; ++i)
    {
        if (soa[i] == *id_tarea_actual && i != n_tasks-1) {
            *id_tarea_actual = soa[i+1];
            return;
        }
    }

    *id_tarea_actual = -1;
}

int tarea_sin_dependencias(int id_tarea_actual, int *realizadas, Task *tasks, int n_asignacion) {
    int n_deps = tasks[id_tarea_actual].n_deps;

    for (int i = 0; i < n_deps; ++i)
    {
        int dep_actual = tasks[id_tarea_actual].dep_tasks[i];

        // si es una tarea distinta a la actual y ...
            // 1.- esta sin completar (n_inst_pendientes > 0)
            // or ...
            // 2.- la tarea de la que depende se completo en el mismo ciclo de asignacion
        // NO se puede realizar (tiene dependencias)
        if (dep_actual!=id_tarea_actual 
            && (realizadas[dep_actual] > 0 || realizadas[dep_actual]*-1 == n_asignacion))   
            return 0;
    }

    return 1;
}

void asignar_recursos(int id_tarea_actual, Platform *optimal_platform, Platform *asignacion, 
    int pos_asignacion, int *cores, int *cores_disponibles, int *realizadas, double *e_acum) {

    int inst_asignar = realizadas[id_tarea_actual];

    int n_devs = optimal_platform->n_devices;
    int dev_act = 0;
    int cores_asignables;
    int id_dev_actual;


    // Comprobamos si se puede hacer uso de hyperthreading
    /*
    * recorrer todos los devs
    *   si cores[i]==dev[i].cores && dev[i].cores*2*inst_core == inst_asignar
    *       usar_hyperthreading(i)
    */
    for (int i = 0; i < n_devs; ++i)
    {
        id_dev_actual = optimal_platform->devices[i].id;
        int n_cores = optimal_platform->devices[i].n_cores;
        int inst_core = optimal_platform->devices[i].inst_core;
        if ((cores[id_dev_actual] == n_cores) && ( n_cores*2*inst_core == inst_asignar)) {

            int index = asignacion[pos_asignacion].n_devices;
            double consumption = optimal_platform->devices[i].consumption;

            asignacion[pos_asignacion].devices[index].id = id_dev_actual;
            asignacion[pos_asignacion].devices[index].n_cores = n_cores;
            asignacion[pos_asignacion].devices[index].inst_core = inst_core;
            asignacion[pos_asignacion].devices[index].consumption = consumption;
            asignacion[pos_asignacion].devices[index].hyperthreading = 1;

            asignacion[pos_asignacion].n_devices = index+1;

            *cores_disponibles -= n_cores;
            cores[id_dev_actual] = 0;   // 0 ya que se usan todos en el hyperthreading

            *e_acum += n_cores * (consumption*1.1);

            realizadas[id_tarea_actual] = 0;
            return;
        }

    }

    while (inst_asignar > 0 && cores_disponibles > 0 && dev_act < n_devs) {

        id_dev_actual = optimal_platform->devices[dev_act].id;
        if ((cores_asignables = cores[id_dev_actual]) > 0)
        {
            int inst_core = optimal_platform->devices[dev_act].inst_core;
            double consumption = optimal_platform->devices[dev_act].consumption;

            double c_asignados_d = (double)inst_asignar/inst_core;
            int c_asignados = (int)ceil(c_asignados_d); // ceil ya que si se necesitan 2.5 => 3

            // controlamos que los cores necesitados no sean mayores que los asignables
            if (c_asignados > cores_asignables)
                c_asignados = cores_asignables;

            inst_asignar -= c_asignados * inst_core;

            int index = asignacion[pos_asignacion].n_devices;

            asignacion[pos_asignacion].devices[index].id = id_dev_actual;
            asignacion[pos_asignacion].devices[index].n_cores = c_asignados;
            asignacion[pos_asignacion].devices[index].inst_core = inst_core;
            asignacion[pos_asignacion].devices[index].consumption = consumption;
            asignacion[pos_asignacion].devices[index].hyperthreading = 0;

            asignacion[pos_asignacion].n_devices = index+1;

            *cores_disponibles -= c_asignados;
            cores[id_dev_actual] -= c_asignados;

            *e_acum += c_asignados * consumption;

        }

        dev_act++;
    }

    realizadas[id_tarea_actual] = inst_asignar;

}

int get_posicion_tarea(int id_tarea_actual, int *soa, int n_tasks) {
    for (int i = 0; i < n_tasks; ++i)
    {
        if (soa[i] == id_tarea_actual)
            return i;
    }

    return -1;
}

int asignar_dispositivos(int *soa, int n_tasks, Task *tasks, Platform *optimal_platform, 
    Platform *selected_dev, double *toa, double *coa) {

    int n_devs = optimal_platform->n_devices;
    int tareas_pendientes = n_tasks;
    int exito = 0;

    Platform *asignacion = (Platform *) malloc(n_tasks*sizeof(Platform));
    assert(selected_dev);

    for(int i=0; i<n_tasks; i++) {
        asignacion[i].devices = (Device *) malloc((2*n_devs)*sizeof(Device));
        asignacion[i].n_devices = 0;    // se inicializa a 0 y se va incrementando en la asignacion
    }

    // Array para controlar las tareas, en cada posicion se guardan las intrucciones a realizar
    // de cada tarea
    int realizadas[n_tasks];
    for (int i = 0; i < n_tasks; ++i)
        realizadas[i] = tasks[i].n_inst;
    
    double t_acum = 0.0, e_acum = 0.0;

    int cores[n_devs];  // cores para cada asignacion
    int cores_disponibles;  // recuento de cuantos cores tenemos por asignar
    int id_tarea_actual = soa[0];   // tarea que se le va a asignar recursos
    // instrucciones restantes de la tarea que se quedo a mitad en la asignacion anterior
    int inst_restantes;
    // 
    int n_asignacion = 0;
    double t_max_it, t_act_it;

    while (tareas_pendientes > 0) {
    
        cores_disponibles = 0;
        t_max_it = 0.0;
        for (int i = 0; i < n_devs; ++i) {
            int id_dev = optimal_platform->devices[i].id;
            cores[id_dev] = optimal_platform->devices[i].n_cores;
            cores_disponibles += cores[id_dev];
        }

        //printf("¿dependencias?: %d\n", tarea_sin_dependencias(id_tarea_actual, realizadas, tasks, n_asignacion));

        while(tareas_pendientes > 0 && cores_disponibles > 0 
                && tarea_sin_dependencias(id_tarea_actual, realizadas, tasks, n_asignacion)){

            inst_restantes = realizadas[id_tarea_actual];
            int inst_totales = tasks[id_tarea_actual].n_inst;

            // el consumo se calcula en los recursos asignados
            int pos_asignacion = get_posicion_tarea(id_tarea_actual, soa, n_tasks);
            asignar_recursos(id_tarea_actual, optimal_platform, asignacion, pos_asignacion,
                    cores, &cores_disponibles, realizadas, &e_acum);


            if (realizadas[id_tarea_actual] <= 0) { // tarea completada
                // guardamos en que asignacion se ha realizado para el control de dependencias
                    // -i indica que en la asignacion i se completo dicha tarea
                realizadas[id_tarea_actual] = -1* n_asignacion;
                tareas_pendientes--;
                siguiente_tarea(&id_tarea_actual, soa, n_tasks);
            } else {
                inst_restantes -= realizadas[id_tarea_actual];
            }

            t_act_it = (double)inst_restantes/inst_totales;
            if (t_act_it > t_max_it)
                t_max_it = t_act_it;

        }

        // se suma el tiempo empleado en el intervalo
        t_acum += t_max_it;
        if (t_acum >= *toa) {
            for(int i=0; i<n_tasks; i++) {
                free(asignacion[i].devices);
            }
            free(asignacion);
            return 0;
        }

        n_asignacion++;

    }

    if ((t_acum < *toa) || (fabs(t_acum - *toa) < 0.001 && e_acum < *coa)) {
        *toa = t_acum;
        *coa = e_acum;
        for(int i=0; i<n_tasks; i++) {
            selected_dev[i].n_devices = asignacion[i].n_devices;
            memcpy(selected_dev[i].devices, asignacion[i].devices, sizeof(Device)*(2*n_devs));
        }

        exito = 1;
    }

    for(int i=0; i<n_tasks; i++) {
        free(asignacion[i].devices);
    }
    free(asignacion);

    return exito;

}

void get_solution(Task *tasks, int n_tasks, Platform *platform, Task *sorted_tasks,
        Platform *selected_dev, double *t, double *e)
{
    int i;
    int nivel;

    // Obtener Numero de Hilos OpenMP
    int num_hilos = atoi(getenv("OMP_NUM_THREADS"));

    double toa[num_hilos];
    double coa[num_hilos];

    int *s[num_hilos];
    int *soa[num_hilos];
    int *done[num_hilos];
    Platform *asignacion[num_hilos];
    int n_devs = platform->n_devices;
	
    // Inicializar Arrays
    for(i = 0; i < num_hilos; i++) {
    	s[i] = (int *) malloc(n_tasks*sizeof(int));
    	memset(s[i], -1, sizeof(int)*n_tasks);
    		
    	soa[i] = (int *) malloc(n_tasks*sizeof(int));
    	memset(soa[i], -1, sizeof(int)*n_tasks);

        done[i] = (int *) malloc(n_tasks*sizeof(int));
        memset(done[i], 0, sizeof(int)*n_tasks);

        toa[i] = coa[i] = DBL_MAX;

        asignacion[i] = (Platform *) malloc(n_tasks*sizeof(Platform));

        for(int j=0; j<n_tasks; j++) {
            asignacion[i][j].devices = (Device *) malloc((2*n_devs)*sizeof(Device));
            asignacion[i][j].n_devices = 0;    // se inicializa a 0 y se va incrementando en la asignacion
        }
    }

    // Calculamos el orden de eficiencia de los dispositivos de la plataforma
        // eficiencia = inst_sec_core/consumo_core
    Platform *optimal_platform = order_by_eficiencia(platform);

    // Paralelizacion con OpenMP
    #pragma omp parallel for private(nivel, i) shared(tasks, n_tasks, optimal_platform,) schedule(dynamic, 1)
    for(i = 0; i < n_tasks; i++) {
        int hilo = omp_get_thread_num();

        memset(done[hilo], 0, sizeof(int)*n_tasks);
        s[hilo][0] = i;
        done[hilo][i] = 1;

        nivel = 0;

        // si no se cumple el criterio para el primer elemento, no se explora nada del subarbol
        if (!criterio(nivel, s[hilo], done[hilo], tasks))
            continue;

    	nivel = 1;

    	while(nivel > 0) {
    		generar(nivel, s[hilo], done[hilo]);
    		if (solucion(nivel, n_tasks, s[hilo], done[hilo], tasks)) {
    			
                // calcular asginacion de dispositivos a dicha secuencia de tareas
                if (asignar_dispositivos(s[hilo], n_tasks, tasks, optimal_platform, 
                        asignacion[hilo], &toa[hilo], &coa[hilo])) {

                    // en toa[hilo] y coa[hilo] tenemos el mejor tiempo y consumo
                    // en soa[hilo] la mejor combinacion
                    // en asignacion[hilo] la mejor asignacion de recursos para la combinacion de soa[hilo]
                    memcpy(soa[hilo], s[hilo], sizeof(int)*n_tasks);
                }

            }

    		if(nivel < n_tasks-1 && criterio(nivel, s[hilo], done[hilo], tasks)) {

    			nivel++;

            } else {
                while (nivel > 0 && !masHermanos(nivel, n_tasks, s[hilo], done[hilo])) {
    				retroceder(nivel, s[hilo], done[hilo]);
                    nivel--;
    			}
    		}
    	}
    }

    // El master se queda con la mejor solucion de las obtenidas por cada hilo
    int mejor_sol = 0;
    for(i = 1; i < num_hilos; i++) {
        if ((toa[i] < toa[mejor_sol]) || (fabs(toa[i] - toa[mejor_sol]) < 0.001 && coa[i] < coa[mejor_sol])) {
            mejor_sol = i;
        }
    }

    *t = toa[mejor_sol];
    *e = coa[mejor_sol];

    for(int i = 0; i < n_tasks; i++) {
        sorted_tasks[i] = tasks[soa[mejor_sol][i]];

        selected_dev[i].n_devices = asignacion[mejor_sol][i].n_devices;
        memcpy(selected_dev[i].devices, asignacion[mejor_sol][i].devices, sizeof(Device)*(2*n_devs));
    }

    // Liberar Memoria
    for(i = 0; i < num_hilos; i++) {
    	free(s[i]);
    	free(soa[i]);
        free(done[i]);
        for(int j=0; j<n_tasks; j++)
            free(asignacion[i][j].devices);
        free(asignacion[i]);
    }

    //free(s);
    //free(soa);
    //free(asignacion);
    free(optimal_platform->devices);
    free(optimal_platform);
}
