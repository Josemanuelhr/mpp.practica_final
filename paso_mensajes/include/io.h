#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "../include/task.h"
#include "../include/platform.h"

Task *load_tasks(const char *, int *, int);
Platform *load_platform(const char *, int);
void print_solution(Task *, int, Platform *, double, double);
void get_solution(Task *, int, Platform *, Task *, Platform *, double *, double *, int, int);