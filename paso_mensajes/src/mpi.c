#include <mpi.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <float.h>


#include "../include/task.h"
#include "../include/platform.h"

void generar(int nivel, int *s, int *done) {
    if (s[nivel] != -1)
        done[s[nivel]] = 0;

    do {
        s[nivel]++;
    } while(done[s[nivel]] == 1);

    done[s[nivel]] = 1;

}

int criterio(int nivel, int *s, int *done, Task *tasks) {
    int task_actual = s[nivel];
    int n_deps = tasks[task_actual].n_deps;

    for (int i = 0; i < n_deps; ++i)
    {
        int dep_actual = tasks[task_actual].dep_tasks[i];
        if (done[dep_actual] == 0)
            return 0;
    }

    return 1;
}

int solucion(int nivel, int n_tasks, int *s, int *done, Task *tasks) {
    return (nivel == n_tasks-1) && criterio(nivel, s, done, tasks);
}

int masHermanos(int nivel, int n_tasks, int *s, int *done) {
    for (int i = s[nivel]+1; i < n_tasks; i++)
        if (done[i] == 0)
            return 1;

    return 0;
}

void retroceder(int nivel, int *s, int *done) {
    done[s[nivel]] = 0;
    s[nivel] = -1;
}

Platform *order_by_eficiencia(Platform *platform) {
    int n_devices = platform->n_devices;

    Platform *res = (Platform *) malloc(sizeof(Platform));
    assert(res);
    res->n_devices = n_devices;
    res->devices = (Device *) malloc(n_devices*sizeof(Device));
    assert(res->devices);

    memcpy(res->devices, platform->devices, sizeof(Device)*n_devices);

    int *inserted = (int *) malloc(n_devices*sizeof(int));
    assert(inserted);
    memset(inserted, 0, sizeof(int)*n_devices);

    int menor_cons;
    double cons_act;
    Device dev;

    for (int i = 0; i < n_devices; ++i)
    {
        menor_cons = i;
        for (int j = i+1; j < n_devices; ++j)
        {
            cons_act = res->devices[j].consumption/res->devices[j].inst_core;

            if (cons_act < res->devices[menor_cons].consumption/res->devices[menor_cons].inst_core) {
                menor_cons = j;
            }
        }

        dev = res->devices[i];
        res->devices[i] = res->devices[menor_cons];
        res->devices[menor_cons] = dev;
    }

    free(inserted);

    return res;
    
}

void siguiente_tarea(int *id_tarea_actual, int *soa, int n_tasks) {
    for (int i = 0; i < n_tasks; ++i)
    {
        if (soa[i] == *id_tarea_actual && i != n_tasks-1) {
            *id_tarea_actual = soa[i+1];
            return;
        }
    }

    *id_tarea_actual = -1;
}

int tarea_sin_dependencias(int id_tarea_actual, int *realizadas, Task *tasks, int n_asignacion) {
    int n_deps = tasks[id_tarea_actual].n_deps;

    for (int i = 0; i < n_deps; ++i)
    {
        int dep_actual = tasks[id_tarea_actual].dep_tasks[i];

        // si es una tarea distinta a la actual y ...
            // 1.- esta sin completar (n_inst_pendientes > 0)
            // or ...
            // 2.- la tarea de la que depende se completo en el mismo ciclo de asignacion
        // NO se puede realizar (tiene dependencias)
        if (dep_actual!=id_tarea_actual 
            && (realizadas[dep_actual] > 0 || realizadas[dep_actual]*-1 == n_asignacion))   
            return 0;
    }

    return 1;
}

void asignar_recursos(int id_tarea_actual, Platform *optimal_platform, Platform *asignacion, 
    int pos_asignacion, int *cores, int *cores_disponibles, int *realizadas, double *e_acum, int rank) {

    int inst_asignar = realizadas[id_tarea_actual];

    int n_devs = optimal_platform->n_devices;
    int dev_act = 0;
    int cores_asignables;
    int id_dev_actual;


    // Comprobamos si se puede hacer uso de hyperthreading
    /*
    * recorrer todos los devs
    *   si cores[i]==dev[i].cores && dev[i].cores*2*inst_core == inst_asignar
    *       usar_hyperthreading(i)
    */
    for (int i = 0; i < n_devs; ++i)
    {
        id_dev_actual = optimal_platform->devices[i].id;
        int n_cores = optimal_platform->devices[i].n_cores;
        int inst_core = optimal_platform->devices[i].inst_core;
        if ((cores[id_dev_actual] == n_cores) && ( n_cores*2*inst_core == inst_asignar)) {

            double consumption = optimal_platform->devices[i].consumption;

            if (rank == 0) {
	            int index = asignacion[pos_asignacion].n_devices;
	            asignacion[pos_asignacion].devices[index].id = id_dev_actual;
	            asignacion[pos_asignacion].devices[index].n_cores = n_cores;
	            asignacion[pos_asignacion].devices[index].inst_core = inst_core;
	            asignacion[pos_asignacion].devices[index].consumption = consumption;
	            asignacion[pos_asignacion].devices[index].hyperthreading = 1;

	            asignacion[pos_asignacion].n_devices = index+1;
	        }

            *cores_disponibles -= n_cores;
            cores[id_dev_actual] = 0;   // 0 ya que se usan todos en el hyperthreading

            *e_acum += n_cores * (consumption*1.1);

            realizadas[id_tarea_actual] = 0;
            return;
        }

    }

    while (inst_asignar > 0 && cores_disponibles > 0 && dev_act < n_devs) {

        id_dev_actual = optimal_platform->devices[dev_act].id;
        if ((cores_asignables = cores[id_dev_actual]) > 0)
        {
            int inst_core = optimal_platform->devices[dev_act].inst_core;
            double consumption = optimal_platform->devices[dev_act].consumption;

            double c_asignados_d = (double)inst_asignar/inst_core;
            int c_asignados = (int)ceil(c_asignados_d); // ceil ya que si se necesitan 2.5 => 3

            // controlamos que los cores necesitados no sean mayores que los asignables
            if (c_asignados > cores_asignables)
                c_asignados = cores_asignables;

            inst_asignar -= c_asignados * inst_core;

            if (rank == 0) {
	            int index = asignacion[pos_asignacion].n_devices;
	            asignacion[pos_asignacion].devices[index].id = id_dev_actual;
	            asignacion[pos_asignacion].devices[index].n_cores = c_asignados;
	            asignacion[pos_asignacion].devices[index].inst_core = inst_core;
	            asignacion[pos_asignacion].devices[index].consumption = consumption;
	            asignacion[pos_asignacion].devices[index].hyperthreading = 0;

	            asignacion[pos_asignacion].n_devices = index+1;
	        }

            *cores_disponibles -= c_asignados;
            cores[id_dev_actual] -= c_asignados;

            *e_acum += c_asignados * consumption;

        }

        dev_act++;
    }

    realizadas[id_tarea_actual] = inst_asignar;

}

int get_posicion_tarea(int id_tarea_actual, int *soa, int n_tasks) {
    for (int i = 0; i < n_tasks; ++i)
    {
        if (soa[i] == id_tarea_actual)
            return i;
    }

    return -1;
}

int asignar_dispositivos(int *soa, int n_tasks, Task *tasks, Platform *optimal_platform, 
    Platform *selected_dev, double *toa, double *coa, int rank) {

    int n_devs = optimal_platform->n_devices;
    int tareas_pendientes = n_tasks;
    int exito = 0;

    Platform *asignacion = (Platform *) malloc(n_tasks*sizeof(Platform));
    assert(selected_dev);

    for(int i=0; i<n_tasks; i++) {
        asignacion[i].devices = (Device *) malloc((2*n_devs)*sizeof(Device));
        asignacion[i].n_devices = 0;    // se inicializa a 0 y se va incrementando en la asignacion
    }

    // Array para controlar las tareas, en cada posicion se guardan las intrucciones a realizar
    // de cada tarea
    int realizadas[n_tasks];
    for (int i = 0; i < n_tasks; ++i)
        realizadas[i] = tasks[i].n_inst;
    
    double t_acum = 0.0, e_acum = 0.0;

    int cores[n_devs];  // cores para cada asignacion
    int cores_disponibles;  // recuento de cuantos cores tenemos por asignar
    int id_tarea_actual = soa[0];   // tarea que se le va a asignar recursos
    // instrucciones restantes de la tarea que se quedo a mitad en la asignacion anterior
    int inst_restantes;
    // 
    int n_asignacion = 0;
    double t_max_it, t_act_it;

    while (tareas_pendientes > 0) {
    
        cores_disponibles = 0;
        t_max_it = 0.0;
        for (int i = 0; i < n_devs; ++i) {
            int id_dev = optimal_platform->devices[i].id;
            cores[id_dev] = optimal_platform->devices[i].n_cores;
            cores_disponibles += cores[id_dev];
        }

        //printf("¿dependencias?: %d\n", tarea_sin_dependencias(id_tarea_actual, realizadas, tasks, n_asignacion));

        while(tareas_pendientes > 0 && cores_disponibles > 0 
                && tarea_sin_dependencias(id_tarea_actual, realizadas, tasks, n_asignacion)){

            inst_restantes = realizadas[id_tarea_actual];
            int inst_totales = tasks[id_tarea_actual].n_inst;

            // el consumo se calcula en los recursos asignados
            int pos_asignacion = get_posicion_tarea(id_tarea_actual, soa, n_tasks);
            asignar_recursos(id_tarea_actual, optimal_platform, asignacion, pos_asignacion,
                    cores, &cores_disponibles, realizadas, &e_acum, rank);


            if (realizadas[id_tarea_actual] <= 0) { // tarea completada
                // guardamos en que asignacion se ha realizado para el control de dependencias
                    // -i indica que en la asignacion i se completo dicha tarea
                realizadas[id_tarea_actual] = -1* n_asignacion;
                tareas_pendientes--;
                siguiente_tarea(&id_tarea_actual, soa, n_tasks);
            } else {
                inst_restantes -= realizadas[id_tarea_actual];
            }

            t_act_it = (double)inst_restantes/inst_totales;
            if (t_act_it > t_max_it)
                t_max_it = t_act_it;

        }

        // se suma el tiempo empleado en el intervalo
        t_acum += t_max_it;
        if (t_acum >= *toa) {
            for(int i=0; i<n_tasks; i++) {
                free(asignacion[i].devices);
            }
            free(asignacion);
            return 0;
        }

        n_asignacion++;

    }

    if ((t_acum < *toa) || (fabs(t_acum - *toa) < 0.001 && e_acum < *coa)) {
        *toa = t_acum;
        *coa = e_acum;
        if (rank == 0) {
	        for(int i=0; i<n_tasks; i++) {
	            selected_dev[i].n_devices = asignacion[i].n_devices;
	            memcpy(selected_dev[i].devices, asignacion[i].devices, sizeof(Device)*(2*n_devs));
	        }
	    }

        exito = 1;
    }

    for(int i=0; i<n_tasks; i++) {
        free(asignacion[i].devices);
    }
    free(asignacion);

    return exito;

}

void get_solution(Task *tasks, int n_tasks, Platform *platform, Task *sorted_tasks,
        Platform *selected_dev, double *t, double *e, int rank, int size)
{

    int nivel;
    double toa, coa;
    toa = coa = DBL_MAX;
    double time_energy[2] = {DBL_MAX, DBL_MAX};

    // Array solucion actual
    int *s = (int *) malloc(n_tasks*sizeof(int));
	assert(s);

	// Se usa para guardar una solucion NO valida
    int *trash_s = (int *) malloc(n_tasks*sizeof(int));
	assert(trash_s);

    // Array de control de que tareas ya estan realizadas
    int *done = (int *) malloc(n_tasks*sizeof(int));
    assert(done);

    // Array solucion optima actual
    int *soa = (int *) malloc(n_tasks*sizeof(int));
    assert(soa);

    // Inicializar Arrays
    memset(s, -1, sizeof(int)*n_tasks);
    memset(done, 0, sizeof(int)*n_tasks);
    memset(soa, -1, sizeof(int)*n_tasks);

    // Calculamos el orden de eficiencia de los dispositivos de la plataforma
    	// eficiencia = inst_sec_core/consumo_core
    Platform *optimal_platform = order_by_eficiencia(platform);

    if (rank == 0) {

    	nivel = 0;
    	int proc_enviar = 1;	// proceso al que enviar la secuencia

		do {
	    	generar(nivel, s, done);
	    	if (solucion(nivel, n_tasks, s, done, tasks)){

	    		// Se envia la secuencia para que un esclavo asigne los dispositivos
	    		MPI_Send(s, n_tasks, MPI_INT, proc_enviar, 0, MPI_COMM_WORLD);
	    		proc_enviar++;

	    	}

	    	if (nivel < n_tasks-1 && criterio(nivel, s, done, tasks)) {

	    		nivel++;

	    	} else {
	    		while (nivel >= 0 && !masHermanos(nivel, n_tasks, s, done)) {
	    			retroceder(nivel, s, done);
	    			nivel--;
	    		}
	    	}

	    	// Se recibe la respuesta de los procesos esclavo, una vez se les ha enviado un trabajo a cada uno
	    	// o justo antes de que se acabe el algoritmo
	    	
	    	if (proc_enviar == size || nivel == -1)
	    	{
	    		int fin_recepcion = proc_enviar;	// puede ser == size o si es fin del algoritmo menor
	    		proc_enviar = 1;

	    		for (int proc = 1; proc < fin_recepcion; ++proc)
	    		{
	    			MPI_Recv(time_energy, 2, MPI_DOUBLE, proc, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	    			if (time_energy[0] < toa || (fabs(time_energy[0] - toa) < 0.001 && time_energy[1] < coa))
	    			{
	    				MPI_Recv(soa, n_tasks, MPI_INT, proc, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	    				toa = time_energy[0];
	    				coa = time_energy[1];

	    			} else {
	    				MPI_Recv(trash_s, n_tasks, MPI_INT, proc, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	    			}
	    			
	    		}
	    	}

    	} while(nivel >= 0);

    	s[0] = -1;	// ponemos el primer elemento a -1 y se lo enviamos a todos los esclavos como señal de fin
    	for (int proc = 1; proc < size; ++proc)
    		MPI_Send(s, n_tasks, MPI_INT, proc, 0, MPI_COMM_WORLD);

    	// Guardamos el mejor, almacenando en selected_dev, sorted_tasks, t y e los resultados
    	*t = DBL_MAX;	// para que al hacer 'asignar_dispositivos' se guarden los resultados
    	asignar_dispositivos(soa, n_tasks, tasks, optimal_platform, selected_dev, t, e, rank);
    	for (int i = 0; i < n_tasks; ++i)
			sorted_tasks[i] = tasks[soa[i]];
		
    } else {

		MPI_Recv(s, n_tasks, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

		while(s[0] != -1) {
			// asignar dispositivos
			asignar_dispositivos(s, n_tasks, tasks, optimal_platform, selected_dev,
					&time_energy[0], &time_energy[1], rank);
            
            // se envia la solucion al master
			MPI_Send(time_energy, 2, MPI_INT, 0, 0, MPI_COMM_WORLD);
			MPI_Send(s, n_tasks, MPI_INT, 0, 0, MPI_COMM_WORLD);
			

			// recibimos s para la siguiente asignacion o para indicarnos el fin
			MPI_Recv(s, n_tasks, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
		}
    }

    free(s);
    free(soa);
    free(done);
    free(trash_s);
    free(optimal_platform->devices);
    free(optimal_platform);
}
