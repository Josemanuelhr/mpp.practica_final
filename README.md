# Jose Manuel Hidalgo Rogel

En esta práctica se propone la implementación de un algoritmo que permita determinar el orden en el que se deben ejecutar un conjunto de tareas teniendo en cuenta las características de los recursos computacionales presentes en una plataforma concreta. El objetivo es conseguir que se ejecuten en el menor tiempo posible reduciendo, a su vez, el consumo de energía.

Se realizaran tres versiones del programa (secuencial, OpenMP y MPI) y un posterior analisis de las prestaciones.
